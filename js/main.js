
$('.top-slider_block').slick({
    fade: true,
    prevArrow: '<button class="btn d-flex justify-content-center align-items-center prev"><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button class="btn d-flex  justify-content-center align-items-center next"><i class="fa fa-angle-right"></i></button>'
});
$('.services-bottom-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    vertical: true,
    verticalScrolling: true,
    arrows: false,
    dots: true,
});
$('.study-block_slider').slick({
    dots: true,
});

$(document).ready(function(){
    $('.portfolio-item').isotope(function(){
        itemSelector:'.item'
    });

    $('.portfolio-menu ul li').click(function(){
        $('.portfolio-menu ul li').removeClass('active');
        $(this).addClass('active');


        var selector = $(this).attr('data-filter');
        $('.portfolio-item').isotope({
            filter: selector
        })
        return false;
    });

////scroll top
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#scroll-top').fadeIn();
        } else {
            $('#scroll-top').fadeOut();
        }
    });
    $('#scroll-top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });


    $(document).on('click','.menu-block ul li a',function (e) {
        e.preventDefault();
        var a = $(this).attr("data-target");
        var b = $("#"+a).scrollTop();
        // console.log('hdhdhdhd')
        console.log(b)
    })
    $('.menu-block ul li a').click(function(event){
        if($( window ).width() > 767) {
            var data = $(this).attr('data-destination-id');
            if ( data ) {
                event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: $('#' + data).offset().top
                }, 'normal');

            }
        }


    });
    ////mobile menu
    $('.menu-mobile').click(function () {
        $('.menu-mobile').toggleClass("active");
        $('.menu-block').toggleClass("active");
        $('body').toggleClass(" overflow-hidden");
        $('.main .top-slider_block .prev, .main .top-slider_block .next').toggleClass('index');
    })

});